//Solves Cryptarithms
using System;
using System.Collections.Generic;

namespace Cryptarithms{
	class MainClass{
		public static void Main (string[] args){
			//string input = "SEND+MORE=MONEY";
			//string input = "AB+CB=BA";
			string input = "THREE+THREE+FIVE=ELEVEN";
			System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
			stopwatch.Start();
			List<string> results = SolveCryptarithmetic(input);
			foreach(string s in results){
				Console.WriteLine(s);
			}
			stopwatch.Stop();
			System.TimeSpan elapsedTime = stopwatch.Elapsed;
			Console.Write(elapsedTime);
		}



		//By using Brute Forece Returns a List of Strings that Solve the Give Problem
		static List<string> SolveCryptarithmetic(string input){
			//Stores All the unique letters in the input string
			List<string> uniqueLetters = FindUniqueLetters(input);
			//How many interations will it take to consider every possiblitiy
			double MaxInterations = System.Math.Pow(10, uniqueLetters.Count);
			//Store the final values
			Console.WriteLine(uniqueLetters.Count);
			Console.WriteLine(MaxInterations);
			List<string> finalResults = new List<string>();
			for (double i = 1; i < MaxInterations; i++){
				//Coverts the number of each iteration to string
				string convertI = i.ToString();
				//Adds a zero infront of each
				while(convertI.Length < uniqueLetters.Count){
					convertI = "0"+convertI;
				}
				//checks if any letter is reapting in the converted string
				if (!IsLetterRepeating(convertI)){
					string newString = input;
					//Repalce letter in the coverted string by numbers
					for(int j=0; j<uniqueLetters.Count; j++){
						if (!newString.Contains(convertI[j].ToString())){
							//stores the new equation
							newString = newString.Replace(uniqueLetters[j], convertI[j].ToString());

						}
					}
					//checks if the equation is correct
					if (CheckEquation(newString)){
						//check if the result is valid
						if (IsValidResult(newString)){
							finalResults.Add(newString);
						}
					}
				}

			}
			return finalResults;

		}

		//returns a list of all the unique letters in the input string
		static List<string> FindUniqueLetters(string input){
			List<string> uniqueLetters = new List<string>();
			for(int i=0; i<input.Length; i++){
				string s = input[i].ToString();
				if (!uniqueLetters.Contains(s) && s!= "+" && s!="="){
					uniqueLetters.Add(s);
				}

			}
			return uniqueLetters;
		}

		//checks if the mathematical equation is true
		static bool CheckEquation(string input){
			string[] splitedinputString = input.Split(new string[] { "="}, System.StringSplitOptions.None);


			string sum = splitedinputString[1];
			//stores the total sum side of the equation
			int actualSum = int.Parse(sum);
			string addingValuesString = splitedinputString[0];

			string[] splitedAddingValues = addingValuesString.Split(new string[] { "+"}, System.StringSplitOptions.None);




			//stores the sum of the values that are being added
			int actualTotal = 0;
			for(int i = 0; i<splitedAddingValues.Length; i++){
				actualTotal += int.Parse(splitedAddingValues[i]);
			}


			if (actualTotal == actualSum){
				return true;
			}else{
				return false;
			}
		}

		//checks if a letter is repeating in a given string, return trues if it is
		static bool IsLetterRepeating(string input){
			bool isRepeating = false;
			for(int i = 0; i < input.Length; i++){
				string[] splitedAddingValues = input.Split(new string[] {input[i].ToString()}, System.StringSplitOptions.None);
				if (splitedAddingValues.Length > 2){
					isRepeating = true;
				}
			}
			return isRepeating;
		}

		//checks if the result is valid, if any elements of the equation starts with "0", returns false
		static bool IsValidResult(string input){
			bool isValid = true;
			string[] splitedinputString = input.Split(new string[] { "="}, System.StringSplitOptions.None);
			if (splitedinputString[1][0].ToString() == "0"){
				isValid = false;
			}

			string addingValuesString = splitedinputString[0];
			string[] splitedAddingValues = addingValuesString.Split(new string[] {"+"}, System.StringSplitOptions.None);
			for(int i = 0; i<splitedAddingValues.Length; i++){
				if (splitedAddingValues[i][0].ToString() == "0"){
					isValid = false;
				}
			}

			return isValid;
		}
	}

}
